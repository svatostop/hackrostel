kubectl apply -f deploy_nginx.yaml --kubeconfig=kubeconfig
kubectl apply -f deploy_ngrok.yaml --kubeconfig=kubeconfig
kubectl get pods -n team3  --kubeconfig=kubeconfig
kubectl exec ngrok --kubeconfig=kubeconfig  -- curl http://localhost:4040/api/tunnels
kubectl delete deployment -n team3 ngrok nginx fast --kubeconfig=kubeconfig
kubectl delete services -n team3 ngrok-service ngrok nginx  nginx-service --kubeconfig=kubeconfig
kubectl delete pods -n team3 ngrok nginx fast --kubeconfig=kubeconfig

