**Команды, которыми мы отфильтровали и получили данные с Hadoop:**

`-> hadoop fs -cp input/*.html filteredhtml`

`-> hadoop fs -get filteredhtml`

____________________________________

**Описание нашего kubernetes-кластера, состоящего из подов: nginx, ngrok, fast-api, react**

**Ссылки на докер-образы:**

https://hub.docker.com/repository/docker/svatostop/nginx_img
https://hub.docker.com/repository/docker/svatostop/fast_img
https://hub.docker.com/repository/docker/svatostop/front_img

**Команды, используемые для реализации:**

`-> kubectl —kubeconfig=kubeconfig —requests="cpu=1,memory=256M" run -it mypod —image=ubuntu — bash`

`-> kubectl run fast --image=svatostop/fast_img:latest --kubeconfig=kubeconfig --requests="0.6,memory=300M"`

`-> kubectl expose pod fast --port=80 --type=NodePort -n team3 --kubeconfig=kubeconfig `


--------------------------------------

(**Как мы пытались до этого** (.yaml файлы под nginx и ngrok лежат в папке _old_))

`-> kubectl apply -f deploy.yaml --kubeconfig=kubeconfig `  (под fast-api)

`-> kubectl apply -f deploy_nginx.yaml --kubeconfig=kubeconfig ` (под nginx)

`-> kubectl apply -f deploy_ngrok.yaml --kubeconfig=kubeconfig `  (под ngrok)

`-> kubectl exec ngrok --kubeconfig=kubeconfig  -- curl http://localhost:4040/api/tunnels `  (разворачиваем nginx под ngrok, на выходе получаем адрес (работает!))

`-> kubectl expose pod fast --port 80 --type=NodePort -n team3 --kubeconfig=kubeconfig `  (разворачиваем fast-api)



